-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Seminario

display.setStatusBar(display.HiddenStatusBar);


--[[
--Background
local background = display.newImage("background.png",display.contentCenterX,display.contentCenterY-15);

-- Quadrado // Pesonagem
local player1 = display.newRect(display.contentCenterX,display.contentCenterY,30,30);


-- Vetor botoes
local buttons = {}

buttons[1] = display.newImage("botoes.jpg",display.contentCenterX+95,display.contentCenterY+160); -- Direcional Pra cima
buttons[1].myName = "up"

buttons[2] = display.newImage("botoes.jpg",display.contentCenterX+95,display.contentCenterY+210); -- Direcional pra baixo
buttons[2].rotation = -180
buttons[2].myName = "down"

buttons[3] = display.newImage("botoes.jpg",display.contentCenterX+45,display.contentCenterY+185);  -- Direcional esquerda
buttons[3].rotation = -90
buttons[3].myName = "left"

buttons[4] = display.newImage("botoes.jpg",display.contentCenterX+140,display.contentCenterY+185); -- Direcional direita
buttons[4].rotation = 90
buttons[4].myName = "right"


-- Criando evento para os botoes

local passosX = 0
local passosY = 0

local touchFunction = function(e)
    if e.phase == "began" or e.phase == "moved" then
        if e.target.myName == "up" then
         passosY = -3
         passosX = 0
        elseif e.target.myName == "down" then
            passosY = 3
            passosX = 0
        elseif e.target.myName == "left" then   
            passosX = -3
            passosY = 0
        elseif e.target.myName == "right" then
            passosX = 3
            passosY = 0
        end
    else
        passosX = 0
        passosY = 0

    end
end

local j=1

for j=1, #buttons do
    buttons[j]:addEventListener("touch", touchFunction)
end



local update = function()
    player1.x = player1.x + passosX
    player1.y = player1.y + passosY
end


-- atuliza jogo
Runtime:addEventListener("enterFrame",update)



]]







--[[


--------------- FISICA ----



    -- LIBRARY --
fisica = require("physics"); -- Declarando biblioteca de fisica  Solar2D
fisica.start();  -- startando ela 




--- CODIGO --

retangulo = display.newRect(display.contentCenterX,display.contentCenterX,30,70)   --- Criando retangulo // newreact(posiçao X , posiçao Y, largura, altura)
retangulo:setFillColor(0,50,255) -- dando (RGB)

fisica.addBody(retangulo,"dynamic",{desity=0,friction=1,bounce=0.4 })     --Adicionando corpo para o retangulo (densidade / atrito / quicar)

-- Criando o Chao

ground = display.newRect(0,500,900,50);   
ground:setFillColor(255,128,0)
fisica.addBody(ground,"static",{friction =1,bounec=0.4})





----------------------- Funcao Drag

local function dragObject( event, params )
	--local body = event.target
	local phase = event.phase
	local stage = display.getCurrentStage()

	if "began" == phase then
		stage:setFocus( retangulo )
		retangulo.isFocus = true
		retangulo.tempJoint = physics.newJoint( "touch", retangulo, event.x, event.y )

	elseif retangulo.isFocus then
		if "moved" == phase then
			retangulo.tempJoint:setTarget( event.x, event.y )

		elseif "ended" == phase or "cancelled" == phase then
			stage:setFocus( nil )
			retangulo.isFocus = false	
			retangulo.tempJoint:removeSelf()

		end
	end

	return true
end

retangulo:addEventListener( "touch", dragObject )



]]







---- Flames ----
-- Sample code by Michael Wilson.

-- Change the background to grey.
display.setDefault( "background", 0.1 )

local x, y = display.contentCenterX, display.contentCenterY -- Source of flame.
local rnd = math.random

-- Run every frame.
local function enterFrame()
	local flame = display.newCircle(x,y, math.random(32,64))
	flame:setFillColor(rnd() + 0.5, rnd() + 0.2, 0)
	flame.blendMode = "add"
	flame.alpha = 0.5

	-- Kill the particle when done.
	local function die()
		display.remove(flame)
	end

	-- Start a transition.
	transition.to(flame, {
		delta = true, -- Move from current location.
		time = 1000, -- In 1.0 seconds.
		x = rnd(-16,16), -- Wiggle.
		y = rnd(-384, -256), -- Go up.
		xScale = -0.9, -- Shrink.
		yScale = -0.9,
		onComplete = die, -- And die.
	})
end

-- Called when a mouse event has been received.
local function mouse( event )
	if event.type ~= "scroll" then -- Check that the user isn't scrolling.
		x, y = event.x or x, event.y or y -- Take a new x,y or keep the old x,y.
	end
end

-- Add the mouse and enterFrame events.
Runtime:addEventListener( "mouse", mouse )
Runtime:addEventListener( "enterFrame", enterFrame )
-- Since mobile devices don't have a mouse, the "mouse" listener from above 
-- won't work with them, but we can add a "touch" listener for such devices.
Runtime:addEventListener( "touch", mouse )
